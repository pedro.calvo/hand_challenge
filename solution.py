import os
os.system("clear")

input_text_file = open("test1.hand", "r")
inp =input_text_file.read()

inp_pointer=0
output_pointer=0
output=[0]
hand_input_list=list(inp)

while inp_pointer!=len(hand_input_list):
    if hand_input_list[inp_pointer]=='👉':
        output_pointer+=1
        inp_pointer+=1
    
    elif hand_input_list[inp_pointer]=='👈':
        output_pointer-=1
        inp_pointer+=1

    elif hand_input_list[inp_pointer]=='👇':
        try:
            if output[output_pointer]==0:
                output[output_pointer]=255
            else:
                output[output_pointer]-=1
            inp_pointer+=1
        except IndexError:
            output.append(0)

    elif hand_input_list[inp_pointer]=='👆':
        try:
            if output[output_pointer]==255:
                output[output_pointer]=0
            else:
                output[output_pointer]+=1
            inp_pointer+=1
        except IndexError:
            output.append(0)

    elif hand_input_list[inp_pointer]=='🤜':
        if output[output_pointer]==0:
            while hand_input_list[inp_pointer]!='🤛':
                inp_pointer+=1
        else:
            inp_pointer+=1

    elif hand_input_list[inp_pointer]=='🤛':
        if output[output_pointer]!=0:
            while hand_input_list[inp_pointer]!='🤜':
                inp_pointer-=1
        else:
            inp_pointer+=1

    elif hand_input_list[inp_pointer]=='👊':
        print(chr(output[output_pointer]),end ='')
        inp_pointer+=1

input_text_file.close()
print("\n")   